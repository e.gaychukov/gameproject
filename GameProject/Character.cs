﻿using System;

namespace GameProject
{
    class Character
    {
        int healthPoints;
        int xPos;
        int yPos;
        char skin;

        public event Action OnMoved;
        public event Action OnDead;

        public Character(int healthPoints, char skin = 'X')
        {
            this.healthPoints = Math.Abs(healthPoints);
            this.skin = skin;
            IsAlive = true;
        }

        public int XPosition => xPos;
        public int YPosition => yPos;
        public char Skin => skin;
        public int HealthPoints => healthPoints;
        public bool IsAlive { get; set; }

        public void Move(Direction direction)
        {
            if(direction == Direction.Up)
            {
                yPos--;
            }
            else if(direction == Direction.Down)
            {
                yPos++;
            }
            else if(direction == Direction.Left)
            {
                xPos--;
            }
            else if(direction == Direction.Right)
            {
                xPos++;
            }
            OnMoved();
        }

        public void Damage(int damagePoints)
        {
            if(damagePoints > 0)
            {
                healthPoints -= damagePoints;
                if(healthPoints <= 0)
                {
                    IsAlive = false;
                    OnDead();
                }
            }
        }
    }
}
