﻿using System;

namespace GameProject
{
    static class Game
    {
        public static void StartGame()
        {
            Character character = new Character(10);
            HealthPointsLabel label = new HealthPointsLabel(character);
            Map map = new Map(character, label);

            map.DrawMap();

            do
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    character.Move(Direction.Up);
                }
                if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    character.Move(Direction.Down);
                }
                if (keyInfo.Key == ConsoleKey.LeftArrow)
                {
                    character.Move(Direction.Left);
                }
                if (keyInfo.Key == ConsoleKey.RightArrow)
                {
                    character.Move(Direction.Right);
                }

                if(character.XPosition == map.MapSize - 1 && character.YPosition == map.MapSize - 1)
                {
                    Utils.DrawLoadPage("Mission completed", ConsoleColor.Green);
                    break;
                }

            } while (character.IsAlive);
        }
    }
}
