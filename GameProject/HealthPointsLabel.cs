﻿using System;

namespace GameProject
{
    class HealthPointsLabel
    {
        Character character;
        int maxHealthPoints;
        public HealthPointsLabel(Character character)
        {
            if(character == null)
            {
                throw new ArgumentNullException("character", "Character's reference is null");
            }
            this.character = character;
            maxHealthPoints = character.HealthPoints;
        }

        public void DrawLabel()
        {
            Console.Write("HP:");

            if(character.HealthPoints >= 0.8 * maxHealthPoints)
            {
                Utils.ColoredWrite(character.HealthPoints, ConsoleColor.Green);
            }
            else if(character.HealthPoints >= 0.4 * maxHealthPoints)
            {
                Utils.ColoredWrite(character.HealthPoints, ConsoleColor.Yellow);
            }
            else
            {
                Utils.ColoredWrite(character.HealthPoints, ConsoleColor.Red);
            }
        }
    }
}
