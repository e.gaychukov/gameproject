﻿using System;

namespace GameProject
{
    class Map
    {
        char backgroundSymbol;
        char[,] mapMatrix;
        int[,] trapsMap;
        Character character;
        HealthPointsLabel label;

        public int MapSize => mapMatrix.GetLength(0);

        public Map(Character character, HealthPointsLabel label, int mapSize = 10, char backgroundSymbol = '*')
        {
            if (character == null)
            {
                throw new ArgumentNullException("character", "Character's reference is null");
            }
            if(label == null)
            {
                throw new ArgumentNullException("label", "Reference is null");
            }
            this.character = character;
            this.backgroundSymbol = backgroundSymbol;
            this.label = label;

            mapSize = Math.Abs(mapSize);

            mapMatrix = new char[mapSize, mapSize];
            trapsMap = new int[mapSize, mapSize];
            
            UpdateMap();
            GenerateTrapsMap();

            character.OnMoved += UpdateMap;
            character.OnMoved += DrawMap;

            character.OnDead += () => Utils.DrawLoadPage("Game Over", ConsoleColor.Red);
        }

        void GenerateTrapsMap()
        {
            int trapsCount = 12;
            Random random = new Random();

            for(int i = 1; i < trapsMap.Length - 1 && trapsCount > 0; i++)
            {
                int y = i / trapsMap.GetLength(1);
                int x = i % trapsMap.GetLength(1);

                int value = random.Next(14); 

                if(value % 12 == 0)
                {
                    trapsMap[y, x] = random.Next(10);
                    trapsCount--;
                }
                else
                {
                    trapsMap[y, x] = 0;
                }
            }
        }

        public void UpdateMap()
        {
            for(int i = 0; i < mapMatrix.GetLength(0); i++)
            {
                for(int j = 0; j < mapMatrix.GetLength(1); j++)
                {
                    mapMatrix[i, j] = backgroundSymbol;
                }
            }

            mapMatrix[mapMatrix.GetLength(0) - 1, mapMatrix.GetLength(1) - 1] = 'P';

            try
            {
                mapMatrix[character.YPosition, character.XPosition] = character.Skin;
                character.Damage(trapsMap[character.YPosition, character.XPosition]);
                trapsMap[character.YPosition, character.XPosition] = 0;
            }
            catch(IndexOutOfRangeException)
            {
                if(character.YPosition >= mapMatrix.GetLength(0))
                {
                    character.Move(Direction.Up);
                }
                if (character.YPosition < 0)
                {
                    character.Move(Direction.Down);
                }
                if(character.XPosition >= mapMatrix.GetLength(1))
                {
                    character.Move(Direction.Left);
                }
                if(character.XPosition < 0)
                {
                    character.Move(Direction.Right);
                }
            }
        }

        public void DrawMap()
        {
            if(Console.WindowWidth < 25)
            {
                Console.SetWindowSize(25, Console.WindowHeight);
            }
            if(Console.WindowHeight < 15)
            {
                Console.SetWindowSize(Console.WindowWidth, 15);
            }

            Console.Clear();

            label.DrawLabel();

            string leftPadding = new string(' ', Console.WindowWidth / 2 - mapMatrix.GetLength(1));
            string topPadding = new string('\n', Console.WindowHeight / 2 - mapMatrix.GetLength(0) / 2);

            Console.Write(topPadding);

            for (int i = 0; i < mapMatrix.GetLength(0); i++)
            {
                Console.Write(leftPadding);
                for (int j = 0; j < mapMatrix.GetLength(1); j++)
                {
                    Console.Write(mapMatrix[i, j].ToString() + ' ');
                }
                Console.WriteLine();
            }
        }
    }
}
