﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameProject
{
    class Menu
    {
        List<string> labels;
        List<Action> labelsActions;
        int currentLabel;

        public Menu()
        {
            labels = new List<string>();
            labelsActions = new List<Action>();

            labels.Add("Exit");
            labelsActions.Add(() => Environment.Exit(0));

            currentLabel = 0;
        }

        public void DrawMenu()
        {
            for (int i = 0; i < labels.Count; i++)
            {
                if (i == currentLabel)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }
                Utils.СenteredWrite(labels[i] + "\n");
                Console.ResetColor();
            }
        }

        public void AddMenuLabel(string labelTitle, Action labelAction)
        {
            if(labelTitle == null)
            {
               throw new ArgumentNullException("labelTitle", "Reference is null");
            }
            if(labelAction == null)
            {
                throw new ArgumentNullException("labelAction", "Reference is null");
            }
            if(labelTitle == "")
            {
                throw new ArgumentException("labelTitle", "Emplty value");
            }

            int labelIndex = labels.FindIndex(el => el == labelTitle);

            if(labelIndex != -1)
            {
                throw new ArgumentException("labelTitle", "Added label title has already exists");
            }

            labels = labels.Prepend(labelTitle).ToList();
            labelsActions = labelsActions.Prepend(labelAction).ToList();
        }

        public void StartMenu()
        {
            do
            {
                Console.Clear();
                DrawMenu();

                ConsoleKeyInfo keyInfo = Console.ReadKey();

                if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    if(currentLabel > 0)
                    {
                        currentLabel--;
                    }
                }
                if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    if(currentLabel < labels.Count - 1)
                    {
                        currentLabel++;
                    }
                }
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    labelsActions[currentLabel]();
                }
            } while (true);
        }
    }
}
