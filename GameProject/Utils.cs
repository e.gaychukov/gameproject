﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace GameProject
{
    static class Utils
    {
        public static void СenteredWrite(object obj)
        {
            int leftPadding = Console.WindowWidth / 2 - obj.ToString().Length / 2;
            string padding = new string(' ', leftPadding);
            Console.Write(padding + obj.ToString());
        }

        public static void DrawLoadPage(string title, ConsoleColor color)
        {
            Console.Clear();
            for(int i = 0; i < title.Length; i++)
            {
                ColoredWrite(title[i], color);
                Thread.Sleep(200);
            }
        }

        public static void ColoredWrite(object obj, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(obj.ToString());
            Console.ResetColor();
        }
    }
}
